const firstNameInput = document.querySelector('#first-name');
const lastNameInput = document.querySelector('#last-name');
const mailInput = document.querySelector('#mail');
const passInput = document.querySelector('#pass');
const statusInput = document.querySelector('select');


document.querySelector('form')
    .addEventListener('submit', function (event) {
        event.preventDefault();
        const newUser = {
            firstName: firstNameInput.value,
            lastName: lastNameInput.value,
            login: mailInput.value,
            password:passInput.value
        };
        fetch('http://localhost:8080/user', {
            method: 'POST',
            body: JSON.stringify(newUser),
            headers: {
                'Content-type': 'application/json'
            }
        }).then(response => {
            if(response.ok){
                console.log("dodano użytkownika ");
               addUserOk()
            }
            else{console.log("błąd!!!");
         addUserError();
        }
    });});

function addUserOk() {
    const myDiv = document.querySelector("#info");
    myDiv.innerHTML = '';
    myDiv.innerHTML = '<p>dodano użytkownika</p>';
}

function addUserError() {
    const myDiv = document.querySelector("#info");
    myDiv.innerHTML = '';
    myDiv.innerHTML = '<p>coś poszło nie tak!</p>'

}
