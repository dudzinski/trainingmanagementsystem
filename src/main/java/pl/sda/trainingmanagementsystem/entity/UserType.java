package pl.sda.trainingmanagementsystem.entity;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum UserType {
    ADMIN(Roles.ADMIN, Roles.SUPER_USER, Roles.BASIC),
    CLIENT(Roles.BASIC),
    LECTOR(Roles.BASIC, Roles.SUPER_USER);

    private final String[] authorities;

    UserType(String... authorities) {
        this.authorities = authorities;
    }

    public Collection<GrantedAuthority> getAuthorities() {
        return Stream.of(authorities)
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }

    public class Roles {
        public static final String ADMIN = "ROLE_ADMIN";
        public static final String BASIC = "ROLE_CLIENT";
        public static final String SUPER_USER = "ROLE_LECTOR";
    }
}
