package pl.sda.trainingmanagementsystem.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import pl.sda.trainingmanagementsystem.entity.UserType;
import pl.sda.trainingmanagementsystem.entity.User;
import pl.sda.trainingmanagementsystem.repository.UserRepository;

import javax.persistence.EntityNotFoundException;
import java.util.Collections;
//@CrossOrigin

@RestController
@RequestMapping("/user")
public class UserController  {

    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    public UserController(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping
    public Iterable<User> getAll() {
        return userRepository.findAll();
    }

    @GetMapping("/{id}")
    public User get(@PathVariable int id) {
        return userRepository.findById(id)
                .orElseThrow(EntityNotFoundException::new);
    }

    @PostMapping
    public User createUser(@RequestBody User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setType(UserType.CLIENT);
        return userRepository.save(user);
    }

    @Secured("ROLE_ADMIN")
    @PostMapping("/lector")
    public User createTrainer(@RequestBody User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setType(UserType.LECTOR);
        return userRepository.save(user);
    }
}
