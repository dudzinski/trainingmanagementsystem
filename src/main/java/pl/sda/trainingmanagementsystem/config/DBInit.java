package pl.sda.trainingmanagementsystem.config;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import pl.sda.trainingmanagementsystem.entity.UserType;
import pl.sda.trainingmanagementsystem.entity.User;
import pl.sda.trainingmanagementsystem.repository.UserRepository;

import javax.annotation.PostConstruct;
import java.util.Collections;


@Component
public class DBInit {
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    public DBInit(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @PostConstruct
    public void onInit() {
        User standardUser = new User("Adam", "Nowak", "client1", passwordEncoder.encode("pass1"),
                UserType.CLIENT);
        User adminUser = new User("Jan", "Kowalski", "admin2", passwordEncoder.encode("pass2"),
                UserType.ADMIN);
        userRepository.save(standardUser);
        userRepository.save(adminUser);
    }
}
